from modloader import BaseMod
from func_timeout import func_timeout, FunctionTimedOut

class HangDectection(BaseMod):
    def initialization(self, generator, story_manager):
        self.inference_timeout = 30
        def bind_act(f):
            def act(action):
                try:
                    return func_timeout(self.inference_timeout, f, (action,))
                except FunctionTimedOut:
                    print(f'That action caused the model to hang. Current timeout is {self.inference_timeout}')
                    return ''
            return act
        story_manager.act = bind_act(story_manager.act)

    def try_handle_command(self, command, args, generator, story_manager):
        if command == 'infto':
            try:
                self.inference_timeout = max(1, int(args[1]))
            except Exception as e:
                print(f'Exception while trying to set inference time: {e}')
            return True
        return False
    
    def instructions(self):
        return ['\n  "/infto x"  Set the inference timeout to `x` seconds.']

